package sahith.algos;

import java.util.Scanner;

public class Sarrays {

	public String reverse() {
		Scanner input = new Scanner(System.in);
		System.out.println("What word do you want to be reversed?");
		String word = input.next();
		int wordlen = word.length();
		char[] wordArray = new char[wordlen];
		word.getChars(0, wordlen, wordArray, 0);
		for (int i = 0; i < wordlen / 2; i++) {
			char temp = wordArray[wordlen - i - 1];
			wordArray[wordlen - i - 1] = wordArray[i];
			wordArray[i] = temp;
		}
		String reversed = new String(wordArray);
		return reversed;
	}

	public boolean anagrams(String word1, String word2) {
		int wordlength1 = word1.length();
		int wordlength2 = word2.length();

		boolean isFound = false;
		if (wordlength2 != wordlength1) {
			return false;
		}
		for (int i = 0; i < wordlength1 - 1; i++) {
			char checkChar = word1.charAt(i);
			for (int j = 0; j < wordlength2 - 1; j++) {
				if (checkChar == word2.charAt(j)) {
					isFound = true;
					break;
				}
			}
			if (isFound == false) {
				return false;
			}
		}
		return true;
	}

	public boolean anagramsV2(String word1, String word2) {
		int w1Array[] = new int[256];
		int w2Array[] = new int[256];

		int wordlength1 = word1.length();
		int wordlength2 = word2.length();
		if (wordlength2 != wordlength1) {
			return false;
		}
		for (int i = 0; i < wordlength1; i++) {
			char ch = word1.charAt(i);
			w1Array[ch]++;
		}

		for (int i = 0; i < wordlength2; i++) {
			char ch = word2.charAt(i);
			w2Array[ch]++;
		}

		for (int i = 0; i < 256; i++) {
			if (w1Array[i] != w2Array[i])
				return false;
		}
		return true;
	}

	public boolean anagramsV3(String word1, String word2) {
		int wArray[] = new int[256];

		int wordlength1 = word1.length();
		int wordlength2 = word2.length();
		if (wordlength2 != wordlength1) {
			return false;
		}

		int numDupes = 0;
		for (int i = 0; i < wordlength1; i++) {
			char ch = word1.charAt(i);
			if (wArray[ch] != 0) {
				numDupes++;
			} else {
				wArray[ch] = 1;
			}
		}

		int num2Dupes = 0;
		for (int i = 0; i < wordlength2; i++) {
			char ch = word2.charAt(i);
			if (wArray[ch] != 0) {

			} else {

			}
		}
		if (numDupes != num2Dupes)
			return false;
		return true;
	}

	public String spaceReplace(String url) {
		char[] urlChars = url.toCharArray();
		StringBuffer buff = new StringBuffer();
		for(int i = 0;i<url.length();i++){
			if(urlChars[i] == ' '){
				buff.append("%20");
			}
			else{
				buff.append(urlChars[i]);
			}
		}
		return buff.toString();
	}

}
