package sahith.algos;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SarraysTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void anagramTest() {
		Sarrays array = new Sarrays();
		boolean result = array.anagrams("abbc", "abbc");
		assert(result);
		
	}
	@Test
	public void spaceTest() {
		Sarrays array = new Sarrays();
		String space = array.spaceReplace("Look at all the spaces");
		assertTrue(space.equals("Look%20at%20all%20the%20spaces"));
	}

}
